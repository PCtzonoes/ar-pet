using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// [RequireComponent(typeof(NavMeshAgent))]
public class PetMovement : MonoBehaviour
{
	private static readonly int Moving = Animator.StringToHash("Moving");

	[SerializeField] private MonoBehaviour lookAt;
	[SerializeField] private float moveSpeed = 2f;
	[SerializeField] private AudioSource move;

	internal Animator Anim;

	// private NavMeshAgent _agent;
	private Vector3 _targetPosition;
	bool _isMoving = false;

	private void Awake()
	{
		// _agent = GetComponent<NavMeshAgent>();
		_targetPosition = transform.position;
	}

	public void SetMovementTarget(in Vector3 hitPosePosition)
	{
		if (_isMoving) return;
		Anim.SetBool(Moving, true);
		_isMoving = true;
		_targetPosition = transform.position;
		lookAt.enabled = false;
		move.Play();
		transform.LookAt(_targetPosition);
		// Handheld.Vibrate();
	}

	private void Update() //TODO: refactor
	{
		if (_isMoving)
		{
			var vec = (_targetPosition - transform.position);

			if (vec.sqrMagnitude < 0.15f)
			{
				// Handheld.Vibrate();
				_isMoving = false;
				Anim.SetBool(Moving, false);
				return;
			}

			transform.position += vec.normalized * moveSpeed * Time.deltaTime;
		}
	}
}