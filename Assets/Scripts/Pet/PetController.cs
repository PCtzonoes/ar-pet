using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PetMovement))]
[RequireComponent(typeof(FeedPet))]
public class PetController : MonoBehaviour
{
	[SerializeField] private Animator animator;
	[SerializeField] private FeedPet feedBehaviour;
	[SerializeField] private PetMovement moveBehaviour;

	[SerializeField] private AudioSource attack;
	[SerializeField] private AudioSource feed;

	private static readonly int _feedHash = Animator.StringToHash("Feed");
	private static readonly int _attackHash = Animator.StringToHash("Attack");

	private void Awake()
	{
		moveBehaviour.Anim = animator;
	}

	public void Feed()
	{
		// if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) return;
		animator.SetTrigger(_feedHash);
		feedBehaviour.FeedNow();
		feed.Play();
	}

	public void Attack()
	{
		animator.SetTrigger(_attackHash);
		attack.Play();
	}

	public void Move(in Vector3 targetPosition)
	{
		moveBehaviour.SetMovementTarget(in targetPosition);
	}
}