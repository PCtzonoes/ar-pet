using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPosition : MonoBehaviour
{
	[SerializeField] private Transform startPosition;
	[SerializeField] private Vector3 offset;

	void Awake()
	{
		transform.position = startPosition.position + offset;
		this.enabled=false;
	}
}