using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class MovePet : MonoBehaviour
{
	[SerializeField] private PetController pet;
	[SerializeField] private GameObject moveIndicator;


	void Awake()
	{
		m_RaycastManager = GetComponent<ARRaycastManager>();

		if (pet != null) return;
		Debug.LogError("Set PetMovement to: ", this);
		gameObject.SetActive(false);
	}

	public void Move(InputAction.CallbackContext context)
	{
		var touchPosition = context.ReadValue<Vector2>();
		if (!m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.Planes)) return;
		// Raycast hits are sorted by distance, so the first one
		// will be the closest hit.
		var hitPose = s_Hits[0].pose;

		moveIndicator.transform.position = hitPose.position;
		pet.Move(hitPose.position);
	}

	static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
	ARRaycastManager m_RaycastManager;
}